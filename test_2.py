import os
import libtmux
import libtmux.exc
import tqdm
import sys


def assertion(session):
    assert session is not None


def start(num_users, base_dir="/home/ubuntu/TPOS/1st/", session_name="jn_session"):
    """
    Запустить $num_users ноутбуков. У каждого рабочая директория $base_dir+$folder_num
    """

    port = 10744
    flag = 0
    server = libtmux.Server()
    session = None
    try:
        session = server.find_where({"session_name": session_name})
    except (libtmux.exc.LibTmuxException, assertion(session)) as e:
        flag = 1
        directory = base_dir + str(1)
        os.system(f'tmux new-session -d -s {session_name} -n window-0 ')
        os.system(f'tmux new-session -d -s {session_name} -n window-0 '
                  f'jupyter notebook --ip 0.0.0.0 --port {port} --no-browser '
                  f'--NotebookApp.notebook_dir={directory}')
        session = server.find_where({"session_name": session_name})
    prev_num = len(session.list_windows())

    for user_id in tqdm.trange(prev_num, prev_num + num_users - flag, file=sys.stdout, desc='starting notebooks'):
        directory = base_dir + str(user_id)
        port = 10744 + user_id
        os.system('mkdir -p %s' % directory)
        os.system(f'tmux new-window -n window-{user_id:d} -t {session_name} '
                  f'jupyter notebook --ip 0.0.0.0 --port {port} --no-browser '
                  f'--NotebookApp.notebook_dir={directory}')


def stop(num, session_name='jn_session'):
    """
    @:param session_name: Названия tmux-сессии, в которой запущены окружения
    @:param num: номер окружения, кот. можно убить
    """

    server = libtmux.Server()
    session = server.find_where({"session_name": session_name})
    session.kill_window("window-%d" % num)


def stop_all(session_name='jn_session'):
    """
    @:param session_name: Название tmux-сессии, в которой запущены окружения
    """

    os.system(f'tmux kill-session -t {session_name}')