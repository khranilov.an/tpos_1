import os
import libtmux
import libtmux.exc
import tqdm
import sys
import argparse


def assertion(session):
    """
    Добавление в исключения случая, когда существует хотя бы одна сессия tmux,
    но нет сессии с необходимым именем
    """

    assert session is not None


def start(num_users, base_dir="/home/ubuntu/TPOS/1st/", session_name="jn_session"):
    """
    Запустить $num_users ноутбуков. У каждого рабочая директория $base_dir+$folder_num
    """

    port = 10744
    flag = 0
    server = libtmux.Server()
    session = None

    try:
        session = server.find_where({"session_name": session_name})
        assertion(session)
    except (libtmux.exc.LibTmuxException, AssertionError):
        for user_id in tqdm.trange(1, file=sys.stdout, desc='starting session'):
            flag = 1
            directory = base_dir + str(0)
            os.system('mkdir -p %s' % directory)
            os.system(f'tmux new-session -d -s {session_name} -n window-0 '
                      f'jupyter notebook --ip 0.0.0.0 --port {port} --no-browser '
                      f'--NotebookApp.notebook_dir={directory}')
            session = server.find_where({"session_name": session_name})
    prev_num = len(session.list_windows())

    for user_id in tqdm.trange(prev_num, prev_num + num_users - flag, file=sys.stdout, desc='starting notebooks'):
        directory = base_dir + str(user_id)
        port = 10744 + user_id
        os.system('mkdir -p %s' % directory)
        os.system(f'tmux new-window -n window-{user_id:d} -t {session_name} '
                  f'jupyter notebook --ip 0.0.0.0 --port {port} --no-browser '
                  f'--NotebookApp.notebook_dir={directory}')


def stop(num, session_name='jn_session'):
    """
    @:param session_name: Названия tmux-сессии, в которой запущены окружения
    @:param num: номер окружения, кот. можно убить
    """

    server = libtmux.Server()
    session = server.find_where({"session_name": session_name})
    session.kill_window("window-%d" % num)


def stop_all(session_name='jn_session'):
    """
    @:param session_name: Название tmux-сессии, в которой запущены окружения
    """

    os.system(f'tmux kill-session -t {session_name}')


"""
Передача аргументов в командную строку:
$ python3 main.py --<имя аргумента> <аргумент> 
Для --stop_all аргумент - любое число
"""

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--start", dest="start", type=int, nargs=1)
parser.add_argument("--stop", dest="stop", type=int, nargs=1)
parser.add_argument("--stop_all", dest="stop_all", type=int, nargs=1)
args = parser.parse_args()
if args.start:
    start(args.start[0])
if args.stop:
    stop(args.stop[0])
if args.stop_all:
    stop_all()
